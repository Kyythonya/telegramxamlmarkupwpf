﻿using System.Collections.Generic;

namespace TelegramXAMLMarkUpWPF
{
    public class User
    {
        public string Name { get; set; }

        public string Image { get; set; }

        public IList<string> Messages { get; set; } = new List<string>();
    }
}
